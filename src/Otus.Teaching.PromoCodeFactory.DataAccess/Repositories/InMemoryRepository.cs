﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = (ICollection<T>)data;
        }
        
        public Task<ICollection<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AppendEntityAsync(T entity)
        {
            Data.Append(entity);
            return Task.CompletedTask;
        }

        public Task EditEntityAsync(T entity)
        {

            T searchedEntity = Data.FirstOrDefault(o => o.Id == entity.Id);
            if (searchedEntity != null)
            {
                searchedEntity = entity;
            }
            return Task.CompletedTask;
        }

        public Task DeleteEntityAsync(T entity)
        {
            Data.Remove(entity);

            return Task.CompletedTask;
        }
    }
}