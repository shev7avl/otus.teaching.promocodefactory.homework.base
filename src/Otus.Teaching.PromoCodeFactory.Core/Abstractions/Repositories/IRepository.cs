﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<ICollection<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        //Homework 1
        Task AppendEntityAsync(T entity);

        Task EditEntityAsync(T entity);

        Task DeleteEntityAsync(T entity);
    }
}