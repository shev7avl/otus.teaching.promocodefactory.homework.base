﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost]
        public async Task<ActionResult> CreateEmployee(string email, string firstName, string lastName)
        {
            IEnumerable<Employee> employees = await _employeeRepository.GetAllAsync();

            Employee employee = new Employee()
            {
                Id = Guid.NewGuid(),
                Email = email,
                Roles = default,
                FirstName = firstName,
                LastName = lastName,
                AppliedPromocodesCount = default
            };

            bool emailExists = employees.Select(o => o.Email).Contains(employee.Email);
            bool fullNameExists = employees.Select(o => o.FullName).Contains(employee.FullName);

            if (emailExists && fullNameExists)
            {
                return BadRequest();
            }

            await _employeeRepository.AppendEntityAsync(employee);

            return CreatedAtAction("AppendEntityAsyn", employee);
        }

        [HttpPut]
        public async Task<ActionResult> EditEmployeeDataById(Guid employeeId, string email, string firstName, string lastName)
        {
            ICollection<Employee> employees = await _employeeRepository.GetAllAsync();

            Employee editedEmployee = employees.FirstOrDefault(o => o.Id == employeeId);

            if (editedEmployee != null)
            {
                editedEmployee.Email = email;
                editedEmployee.FirstName = firstName;
                editedEmployee.LastName = lastName;

                await _employeeRepository.EditEntityAsync(editedEmployee);
                return Ok();
            }
            
            return BadRequest();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteById(Guid id)
        {
            IEnumerable<Employee> employees = await _employeeRepository.GetAllAsync();

            Employee employeeToDelete = employees.FirstOrDefault(o => o.Id == id);

            if (employeeToDelete != null)
            {
                await _employeeRepository.DeleteEntityAsync(employeeToDelete);

                return Ok();
            }

            return BadRequest();

        }
    }
}